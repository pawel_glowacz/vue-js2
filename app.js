var data = {
    name:'Yoshi'
}
Vue.component('greeting',{
    template:'<p>Greets from compo {{name}}. <button v-on:click="changeName">Change Name</button></p>',
    data:function(){
        return{
            name:"Yoshi"
        }
    },
    methods:{
        changeName:function() {
            this.name = "Mario"
        }
    }

});
var one = new Vue({
    el:"#vue-app-one",
    data:{
        title:"Vue app one",
        // name:'Shaun',
        job:'Ninja',
        website:'https://www.google.pl',
        websiteTag:'<a href="https://www.google.pl" >Website</a>',
        age:25,
        x:0,
        y:0,
        name:"",
        // age:"",
        a:0,
        b:0,
        available:false,
        nearby:false,
        error:false,
        success:false,
        characters:['mario','luigi','yoshi','bowser'],
        ninjas:[
            {name:'Ryu',age:25},
            {name:'Yoshi',age:35},
            {name:'Ken',age:55}
        ],
        health:100,
        start:false,
        ended:false
    },
    methods:{
        punch:function(){
          this.health -= 10;
          console.log(this.health);
          if(this.health <= 0){
              this.ended= true;
          }
        },
        restart:function(){
          this.health=100;
          this.ended=false;
        },
        greet:function(time){
            return "Good " + time + this.name;
        },
        add: function(inc){
            this.age += inc;
        },
        substract: function(dec){
            this.age -= dec;
        },
        updateXY: function(event){
            this.x = event.offsetX;
            this.y = event.offsetY;
        },
        click:function() {
            alert("You clicked me :");
        },
        logName: function(){
            // console.log('YOu entered your name');
        },
        logAge: function(){
            // console.log('You entered your age')
        },
        // addToA: function(){
        //     console.log("addToA");
        //     return this.a + this.age;
        // },
        // addToB: function(){
        //     console.log("addToB");
        //     return this.b + this.age;
        // }
    },
    computed:{
        addToA: function(){
            console.log("addToA");
            return this.a + this.age;
        },
        addToB: function(){
            console.log("addToB");
            return this.b + this.age;
        },
        compClasses: function(){
            return{
                available:this.available,
                nearby:this.nearby
            }
        },
        greets:function(){
            return "App one dude";
        }
    }
});
var two = new Vue({
    el:"#vue-app-two",
    data:{
        title:"Vue app two",
        output:"YOur fav food"
    },
    methods: {
        changeTitle: function(){
            one.title = "Title one changed";
        },
        readRefs:function(){
            console.log(this.$refs.test.innerText);
            this.output = this.$refs.input.value;
        }
    },
    computed:{
        greets:function(){
            return "App two dude";
        }
    }
});

two.title = "change from outside";